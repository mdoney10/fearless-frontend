/**
 * This script is used to create a new conference.
 * It is used by the new-conference.html page.
 */

/**
 * This function is called when the form is submitted.
 * It retrieves the data from the form and sends it to the API.
 */
document.addEventListener('DOMContentLoaded', async function () {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    const getId = (url) => {
        const comps = url.split('/');
        return comps[comps.length - 2];
    }
    if (response.ok) {
        const responseData = await response.json();
        const element = document.querySelector("#location");
        // load options into the select element
        for (let location of responseData.locations) {
            const option = document.createElement("option");
            option.text = location.name;
            option.value = getId(location.href);
            element.add(option);
        }
    }
    // add an event listener for the submit event from the 'create-conference-form' form.
    const form = document.querySelector("#create-conference-form");
    form.addEventListener('submit', async (event) => {
        event.preventDefault();
        // do a POST request to the API
        const formData = new FormData(form);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            form.reset();
            const newConference = await response.json();
        }
    });
});
