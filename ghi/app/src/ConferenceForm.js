import React, { useEffect, useState } from 'react';

/*
To create a new conference, the API expects data in the following format:

{
  url => 'http://localhost:8000/api/conferences/'
  "name": "AI & Robotics Conference",
  "starts": "2025-03-24",
  "ends": "2025-03-26",
  "description": "On behalf of the Organizing Committee, we are ecstatic to invite you to 'Tech Summit on Artificial Intelligence & Robotics'",
  "max_presentations": 70,
  "max_attendees": 10000,
  "location": 2
}
*/


function ConferenceForm() {

  // locations
  const [locations, setLocations] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  // name
  const [name, setName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  // starts
  const [starts, setStarts] = useState('');
  const handleStartsChange = (event) => {
    const value = event.target.value;
    setStarts(value);
  }

  // ends
  const [ends, setEnds] = useState('');
  const handleEndsChange = (event) => {
    const value = event.target.value;
    setEnds(value);
  }

  // description
  const [description, setDescription] = useState('');
  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  // max presentations
  const [maxPresentations, setMaxPresentations] = useState('');
  const handleMaxPresentationsChange = (event) => {
    const value = event.target.value;
    setMaxPresentations(value);
  }

  // max attendees
  const [maxAttendees, setMaxAttendees] = useState('');
  const handleMaxAttendeesChange = (event) => {
    const value = event.target.value;
    setMaxAttendees(value);
  }

  // location
  const [location, setLocation] = useState('');
  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  // submit
  const handleSubmit = async (event) => {
    const getId = (url) => {
      const comps = url.split('/');
      return comps[comps.length - 2];
    }
    event.preventDefault();
    const url = 'http://localhost:8000/api/conferences/';
    const data = {
      name: name,
      starts: starts,
      ends: ends,
      description: description,
      max_presentations: maxPresentations,
      max_attendees: maxAttendees,
      location: getId(location)
    };
    const fetchConfig = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data)
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      setName('');
      setStarts('');
      setEnds('');
      setDescription('');
      setMaxPresentations('');
      setMaxAttendees('');
      document.getElementById('create-conference-form').reset();
    } else {
      console.log('Error creating conference');
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name"
                className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStartsChange} placeholder="Starts" required type="date" id="starts" name="starts"
                className="form-control" />
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEndsChange} placeholder="Ends" required type="date" id="ends" name="ends"
                className="form-control" />
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description" className="form-label">Description</label>
              <textarea onChange={handleDescriptionChange} placeholder="Description" required id="description" rows="3"
                className="form-control" name="description"></textarea>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxPresentationsChange} placeholder="Maximum presentations" required type="number" id="max_presentations"
                name="max_presentations" className="form-control" />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxAttendeesChange} placeholder="Maximum attendees" required type="number" id="max_attendees"
                name="max_attendees" className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select onChange={handleLocationChange} required id="location" className="form-select" name="location">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.href} value={location.href}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
