import React, { useEffect, useState } from 'react';

function LocationForm() {

  // states
  const [states, setStates] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setStates(data.states);
    }
  }

  // name
  const [name, setName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  // room count
  const [roomCount, setRoomCount] = useState('');
  const handleRoomCountChange = (event) => {
    const value = event.target.value;
    setRoomCount(value);
  };

  // city
  const [city, setCity] = useState('');
  const handleCityChange = (event) => {
    const value = event.target.value;
    setCity(value);
  };

  // state
  const [state, setState] = useState('');
  const handleStateChange = (event) => {
    const value = event.target.value;
    setState(value);
  };

  // submit
  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8000/api/locations/';
    const data = {
      name: name,
      room_count: roomCount,
      city: city,
      state: state
    };
    const fetchConfig = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data)
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      setName('');
      setRoomCount('');
      setCity('');
      setState('');
      document.getElementById('create-location-form').reset();
    } else {
      console.log('Error creating location');
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name"
                className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleRoomCountChange} placeholder="Room count" required type="number" id="room_count" name="room_count"
                className="form-control" />
              <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleCityChange} placeholder="City" required type="text" id="city" className="form-control"
                name="city" />
              <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
              <select onChange={handleStateChange} required id="state" className="form-select" name="state">
                <option value="">Choose a state</option>
                {states.map(state => {
                  return (
                    <option key={state.abbreviation} value={state.abbreviation}>
                      {state.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default LocationForm;
